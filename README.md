Editor3D
========

This project is a simple 3D editor coded for my Computer Graphics course.
It can read simple .OBJ file, perform simple vertex operation, and can render
to POV-Ray.

