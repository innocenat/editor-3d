#pragma once

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>
#include <cstdio>

class CamControl {
public:

  explicit CamControl(GLFWwindow* windows)
    : windows(windows) {
  }

  virtual ~CamControl() {}

  void computeMatricesFromInput();
  glm::mat4 getProjectionMatrix();
  glm::mat4 getViewMatrix();

  void outputToPov(FILE* fp) const {
    auto look_at = position + direction;
    fprintf(fp, "camera {\n");
    fprintf(fp, "\tlocation <%f, %f, %f>\n", position.x, position.y, position.z);
    fprintf(fp, "\tup <%f, %f, %f>\n", up.x, up.y, up.z);
    fprintf(fp, "\tright <%f, %f, %f>\n", -right.x, -right.y, -right.z);
    fprintf(fp, "\tlook_at <%f, %f, %f>\n", look_at.x, look_at.y, look_at.z);
    fprintf(fp, "\tangle 70\n");
    fprintf(fp, "}\n\n");
    fprintf(fp, "light_source { <%f, %f, %f> color <1,1,1> }\n\n", position.x, position.y, position.z);
  }
private:
  GLFWwindow* windows;

  double lastTime = 0.;

  // position
  glm::vec3 position = glm::vec3(0, 0, 5);
  glm::vec3 direction = glm::vec3(1,0,0), right = glm::vec3(1,0,0), up = glm::vec3(0,1,0);

  float mXpos, mYpos;
  bool isMoving = false;

  // horizontal angle : toward -Z
  float horizontalAngle = 3.14f;
  // vertical angle : 0, look at the horizon
  float verticalAngle = 0.0f;
  // Initial Field of View
  float initialFoV = 45.0f;

  float speed = 3.0f; // 3 units / second
  float mouseSpeed = 0.5f;
};
