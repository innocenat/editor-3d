// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "controls.h"
#include <stdio.h>

void CamControl::computeMatricesFromInput() {
  auto currentTime = glfwGetTime();
  float deltaTime = float(currentTime - lastTime);
  lastTime = currentTime;

  bool leftMouse = glfwGetMouseButton(windows, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS;
  bool middleMouse = glfwGetMouseButton(windows, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS;

  if (leftMouse || middleMouse) {
    double xpos, ypos;
    glfwGetCursorPos(windows, &xpos, &ypos);

    if (!isMoving) {
      mXpos = xpos;
      mYpos = ypos;
    }


    if (leftMouse)  {
      // Compute new orientation
      horizontalAngle += mouseSpeed * deltaTime * float(mXpos - xpos);
      verticalAngle += mouseSpeed * deltaTime * float(mYpos - ypos);

      // Direction : Spherical coordinates to Cartesian coordinates conversion
      direction = glm::vec3(
        cos(verticalAngle) * sin(horizontalAngle),
        sin(verticalAngle),
        cos(verticalAngle) * cos(horizontalAngle)
        );

      // Right vector
      right = glm::vec3(
        sin(horizontalAngle - 3.14f / 2.0f),
        0,
        cos(horizontalAngle - 3.14f / 2.0f)
        );

      // Up vector : perpendicular to both direction and right
      up = glm::cross(right, direction);
    }

    if (middleMouse) {
      position -= up * deltaTime * speed * float(mYpos - ypos);
      position += right * deltaTime * speed * float(mXpos - xpos);
    }

    mXpos = xpos;
    mYpos = ypos;
    isMoving = true;
  } else {
    isMoving = false;
  }

  // Move forward
  if (glfwGetKey(windows, GLFW_KEY_UP) == GLFW_PRESS){
    position += direction * deltaTime * speed;
  }
  // Move backward
  if (glfwGetKey(windows, GLFW_KEY_DOWN) == GLFW_PRESS){
    position -= direction * deltaTime * speed;
  }
  // Strafe right
  if (glfwGetKey(windows, GLFW_KEY_RIGHT) == GLFW_PRESS){
    position += right * deltaTime * speed;
  }
  // Strafe left
  if (glfwGetKey(windows, GLFW_KEY_LEFT) == GLFW_PRESS){
    position -= right * deltaTime * speed;
  }
}

glm::mat4 CamControl::getProjectionMatrix() {
  return glm::perspective(initialFoV, 4.0f / 3.0f, 0.1f, 100.0f);
}

glm::mat4 CamControl::getViewMatrix() {
  return glm::lookAt(
    position,             // Camera is here
    position + direction, // and looks here : at the same position, plus "direction"
    up                    // Head is up (set to 0,-1,0 to look upside-down)
  );
}
