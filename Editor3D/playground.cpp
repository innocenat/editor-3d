#define _CRT_SECURE_NO_WARNINGS

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include <string>
#include <vector>
#include <fstream>

// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#define GLEW_STATIC
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

#include "commondialog.h"
#include "controls.h"
#include "object.h"

GLuint LoadShaders(const char * vertex_file_path, const char * fragment_file_path){

  // Create the shaders
  GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  // Read the Vertex Shader code from the file
  std::string VertexShaderCode;
  std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
  if (VertexShaderStream.is_open())
  {
    std::string Line = "";
    while (getline(VertexShaderStream, Line))
      VertexShaderCode += "\n" + Line;
    VertexShaderStream.close();
  }

  // Read the Fragment Shader code from the file
  std::string FragmentShaderCode;
  std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
  if (FragmentShaderStream.is_open()){
    std::string Line = "";
    while (getline(FragmentShaderStream, Line))
      FragmentShaderCode += "\n" + Line;
    FragmentShaderStream.close();
  }

  GLint Result = GL_FALSE;
  int InfoLogLength;

  // Compile Vertex Shader
  printf("Compiling shader : %s\n", vertex_file_path);
  char const * VertexSourcePointer = VertexShaderCode.c_str();
  glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
  glCompileShader(VertexShaderID);

  // Check Vertex Shader
  glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  std::vector<char> VertexShaderErrorMessage(InfoLogLength);
  glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
  fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);

  // Compile Fragment Shader
  printf("Compiling shader : %s\n", fragment_file_path);
  char const * FragmentSourcePointer = FragmentShaderCode.c_str();
  glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
  glCompileShader(FragmentShaderID);

  // Check Fragment Shader
  glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
  glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
  glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
  fprintf(stdout, "%s\n", &FragmentShaderErrorMessage[0]);

  // Link the program
  fprintf(stdout, "Linking program\n");
  GLuint ProgramID = glCreateProgram();
  glAttachShader(ProgramID, VertexShaderID);
  glAttachShader(ProgramID, FragmentShaderID);
  glLinkProgram(ProgramID);

  // Check the program
  glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
  glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
  std::vector<char> ProgramErrorMessage(max(InfoLogLength, int(1)));
  glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
  fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);

  glDeleteShader(VertexShaderID);
  glDeleteShader(FragmentShaderID);

  return ProgramID;
}

int main() {
  // Initialise GLFW
  if (!glfwInit())
  {
    fprintf(stderr, "Failed to initialize GLFW\n");
    return -1;
  }

  glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3 
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

  // Open a window and create its OpenGL context 
  GLFWwindow* window; // (In the accompanying source code, this variable is global) 
  window = glfwCreateWindow(1024, 768, "3D Editor", NULL, NULL);


  if (window == NULL){
    fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window); // Initialize GLEW 
  glewExperimental = true; // Needed in core profile 
  if (glewInit() != GLEW_OK) {
    fprintf(stderr, "Failed to initialize GLEW\n");
    return -1;
  }

  CamControl camControl(window);

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS | GLFW_STICKY_MOUSE_BUTTONS, GL_TRUE);
  glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

  // Enable depth test & face culling
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_CULL_FACE);
  // Accept fragment if it closer to the camera than the former one
  glDepthFunc(GL_LESS);


  // VertexArray
  GLuint VertexArrayID;
  glGenVertexArrays(1, &VertexArrayID);
  glBindVertexArray(VertexArrayID);

  // Color Buffer
  GLuint ColorBuffer;
  glGenBuffers(1, &ColorBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer);

  // Color Buffer
  GLuint NormalBuffer;
  glGenBuffers(1, &NormalBuffer);
  glBindBuffer(GL_ARRAY_BUFFER, NormalBuffer);

  // This will identify our vertex buffer
  GLuint VertexBuffer;
  glGenBuffers(1, &VertexBuffer);

  // Create and compile our GLSL program from the shaders
  GLuint programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");

  // Get a handle for our "MVP" uniform.
  // Only at initialisation time.
  GLuint MatrixID = glGetUniformLocation(programID, "MVP");
  GLuint Matrix2ID = glGetUniformLocation(programID, "MV");
  GLuint Matrix3ID = glGetUniformLocation(programID, "V");
  GLuint NormalMatrixID = glGetUniformLocation(programID, "normalMatrix");
  GLuint
    RedPosition = glGetUniformLocation(programID, "rL"),
    GreenPosition = glGetUniformLocation(programID, "gL"),
    BluePosition = glGetUniformLocation(programID, "bL");

  std::vector<Object> objects;

  bool verticeMode = false, wireframeMode = false, vMoveMode = false;
  bool vPushed = false, clicked = false;
  std::vector<size_t> selected_node;
  size_t cur_obj = -1;
  double mouseX, mouseY;
  float angle = 0;
  do {
    // Clear scene
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if (!verticeMode)
      camControl.computeMatricesFromInput();
    
    const auto MVmat = camControl.getProjectionMatrix() * camControl.getViewMatrix();

    // Object selection
    if (verticeMode && glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {
      if (!clicked) {
        clicked = true;
        glfwGetCursorPos(window, &mouseX, &mouseY);
        mouseX = mouseX / 1024.f * 2.f - 1.f;
        mouseY = -(mouseY / 768.f * 2.f - 1.f);

        selected_node.clear();
        if (cur_obj != -1)
          objects[cur_obj].highlight(selected_node);
        cur_obj = -1;
        float min_dist = 1e38;
        for (int i = 0; i < objects.size(); i++) {
          float cur_dist;
          auto b = objects[i].findVertexAtMouse(MVmat, vec2(mouseX, mouseY), cur_dist);
          if (b != -1 && (i == 0 || cur_dist < min_dist)) {
            selected_node = objects[i].findAllSameVertex(b);
            cur_obj = i;
            min_dist = cur_dist;
          }
        }
        if (cur_obj != -1)
          objects[cur_obj].highlight(selected_node);
      }
      if (vMoveMode) {
        printf("In Move\n");
        double newX, newY;
        glfwGetCursorPos(window, &newX, &newY);
        newX = newX / 1024.f * 2.f - 1.f;
        newY = -(newY / 768.f * 2.f - 1.f);

        if (selected_node.size() > 0) {
          auto vtx = vec4(objects[cur_obj].vertex[selected_node[0]], 1);
          auto pvtx = MVmat * objects[cur_obj].model * vtx;
          //const auto ww = pvtx.w;
          pvtx /= pvtx.w;
          pvtx.x = newX;
          pvtx.y = newY;
          
          auto rvtx = inverse(MVmat * objects[cur_obj].model) * pvtx;
          rvtx /= rvtx.w;
          
          for (const auto& idx : selected_node) {
            objects[cur_obj].vertex[idx] = vec3(rvtx.x, rvtx.y, rvtx.z);
          }
        }

        mouseX = newX;
        mouseY = newY;
      }
    }
    if (!glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT))
      clicked = false;

    glUseProgram(programID);

    const float diff = 2.f * 3.14159265f / 3.f;
    glUniform3f(RedPosition, 100*sin(angle), 100*cos(angle), 0);
    glUniform3f(GreenPosition, 100 * sin(-angle), 100 * cos(-angle), 0);
    glUniform3f(BluePosition, 100 * sin(2*angle), 100 * cos(2*angle), 0);

    angle += 0.005;

    for (const auto& o : objects) {
      auto V = camControl.getViewMatrix();
      auto MVP = MVmat * o.model;
      auto MV = V * o.model;
      auto NormalMatrix = transpose(inverse(MV));
      glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
      glUniformMatrix4fv(Matrix2ID, 1, GL_FALSE, &MV[0][0]);
      glUniformMatrix4fv(Matrix3ID, 1, GL_FALSE, &V[0][0]);
      glUniformMatrix4fv(NormalMatrixID, 1, GL_FALSE, &NormalMatrix[0][0]);

      // Upload buffer
      glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
      glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * o.vertex.size(), &o.vertex[0], GL_STATIC_DRAW);
      glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer);
      glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * o.color.size(), &o.color[0], GL_STATIC_DRAW);
      glBindBuffer(GL_ARRAY_BUFFER, NormalBuffer);
      glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * o.normal.size(), &o.normal[0], GL_STATIC_DRAW);

      glEnableVertexAttribArray(0);
      glBindBuffer(GL_ARRAY_BUFFER, VertexBuffer);
      glVertexAttribPointer(
        0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*) 0            // array buffer offset
        );

      glEnableVertexAttribArray(1);
      glBindBuffer(GL_ARRAY_BUFFER, ColorBuffer);
      glVertexAttribPointer(
        1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*) 0                          // array buffer offset
        );

      glEnableVertexAttribArray(2);
      glBindBuffer(GL_ARRAY_BUFFER, NormalBuffer);
      glVertexAttribPointer(
        2,                                // attribute. No particular reason for 1, but must match the layout in the shader.
        3,                                // size
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                                // stride
        (void*) 0                          // array buffer offset
        );

      // Draw the triangle !
      glDrawArrays(GL_TRIANGLES, 0, 3 * o.vertex.size()); // Starting from vertex 0; 3 vertices total -> 1 triangle

      glDisableVertexAttribArray(0);
      glDisableVertexAttribArray(1);
      glDisableVertexAttribArray(2);
    }

    // Swap buffers
    glfwSwapBuffers(window);
    glfwPollEvents();

    // O = add new model
    if (glfwGetKey(window, GLFW_KEY_O)) {
      // Add new model
      char name[260];
      if (showOpenFileDialog(name)) {
        Object o;
        o.loadFromFile(name);

        // Use command line interface
        printf("Enter position (x,y,z): ");
        float x, y, z;
        scanf("%f %f %f", &x, &y, &z);

        o.model = glm::translate(glm::vec3(x, y, z));

        objects.push_back(o);

        printf("Object is loaded, id = %llu\n", objects.size());
      }

    }
    // Q = render
    if (glfwGetKey(window, GLFW_KEY_Q)) {
      FILE* fp = fopen("D:\\t\\output.pov", "w");
      camControl.outputToPov(fp);
      fprintf(fp, "background{ rgb<0, 0, 0.5> }\n\n");
      fprintf(fp, "plane { y, -5 pigment { checker rgb<0,0,0>, rgb<1,1,1> } }\n\n");
      for (const auto& x : objects) {
        x.exportToPOV(fp, false);
      }
      fclose(fp);
      system("start pvengine64.exe +w640 +h480 +a0.3 +am2 /RENDER \"D:\\t\\output.pov\"");
      Sleep(1500);
    }
    // G = render
    if (glfwGetKey(window, GLFW_KEY_G)) {
      FILE* fp = fopen("D:\\t\\output.pov", "w");
      camControl.outputToPov(fp);
      fprintf(fp, "background{ rgb<0, 0, 0.5> }\n\n");
      fprintf(fp, "plane { y, -5 pigment { checker rgb<0,0,0>, rgb<1,1,1> } }\n\n");
      for (const auto& x : objects) {
        x.exportToPOV(fp, true);
      }
      fclose(fp);
      system("start pvengine64.exe +w640 +h480 +a0.3 +am2 /RENDER \"D:\\t\\output.pov\"");
      Sleep(1500);
    }
    // R = rotate model
    if (glfwGetKey(window, GLFW_KEY_R)) {
      printf("Enter ID [rotate]: ");
      int id;
      scanf("%d", &id);
      id--;
      if (id < 0 || id >= objects.size()) {
        printf("Invalid object id!");
      } else {
        printf("Enter axis & angle (x y z w): ");
        float x, y, z, w;
        scanf("%f%f%f%f", &x, &y, &z, &w);
        objects[id].model = glm::rotate(w, vec3(x, y, z)) * objects[id].model;
      }
    }
    // T = translate model
    if (glfwGetKey(window, GLFW_KEY_T)) {
      printf("Enter ID [translate]: ");
      int id;
      scanf("%d", &id);
      id--;
      if (id < 0 || id >= objects.size()) {
        printf("Invalid object id!");
      } else {
        printf("Enter vector (x y z): ");
        float x, y, z;
        scanf("%f%f%f", &x, &y, &z);
        objects[id].model = glm::translate(vec3(x, y, z)) * objects[id].model;
      }
    }
    // E = edit model
    if (wireframeMode && glfwGetKey(window, GLFW_KEY_E)) {
      verticeMode = true;
    }
    if (verticeMode && glfwGetKey(window, GLFW_KEY_M)) {
      vMoveMode = true;
    } else if (verticeMode && !glfwGetKey(window, GLFW_KEY_M)) {
      vMoveMode = false;
    }
    if (verticeMode && glfwGetKey(window, GLFW_KEY_C)) {
      printf("Enter new vertice color (r g b)[0-1]: ");
      float r, g, b;
      scanf("%f%f%f", &r, &g, &b);

      if (selected_node.size() > 0) {
        
        for (const auto& idx : selected_node) {
          objects[cur_obj].color2[idx] = vec3(r,g,b);
        }
      }

    }
    if (verticeMode && glfwGetKey(window, GLFW_KEY_ENTER)) {
      verticeMode = false;
      vMoveMode = false;

      selected_node.clear();
      if (cur_obj != -1)
        objects[cur_obj].highlight(selected_node);
    }

    // V = switch to wireframe
    if (!vPushed && glfwGetKey(window, GLFW_KEY_V)) {
      vPushed = true;
      if (!wireframeMode) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        wireframeMode = true;
        for (auto& x : objects)
          x.toWireframe();
      } else {
        wireframeMode = false;
        verticeMode = false;
        vMoveMode = false;
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        for (auto& x : objects)
          x.noWireframe();
      }
    } else if (!glfwGetKey(window, GLFW_KEY_V)) vPushed = false;

  } // Check if the ESC key was pressed or the window was closed
  while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);

  return 0;
}
