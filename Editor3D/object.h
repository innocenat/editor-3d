#pragma once

#include <vector>
#include <glm/glm.hpp>

class Object {
public:
  Object() {}
  virtual ~Object() {}

  bool loadFromFile(const char* filename);

  size_t findVertexAtMouse(glm::mat4 MV, glm::vec2 pos, float&);
  std::vector<size_t> findAllSameVertex(size_t index);
  
  void toWireframe() {
    if (!isWireframe) {
      std::swap(color, color2);
      isWireframe = true;
    }
  }

  void noWireframe() {
    if (isWireframe) {
      std::swap(color, color2);
      isWireframe = false;
    }
  }

  void highlight(std::vector<size_t> v) {
    if (isWireframe) {
      for (int i = 0; i < color.size(); i++) {
        color[i] = glm::vec3(0.1f, 0.1f, 0.1f);
      }
      for (const size_t& vv : v) {
        color[vv] = glm::vec3(1.f, 1.f, 1.f);
      }
    }
  }

  void exportToPOV(FILE*, bool g) const;

  bool isWireframe = false;

  std::vector<glm::vec3> vertex;
  std::vector<glm::vec3> color;
  std::vector<glm::vec3> color2;
  std::vector<glm::vec3> normal;
  std::vector<glm::vec2> uv;
  glm::mat4 model = glm::mat4(1.f);
};
