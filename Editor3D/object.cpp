#define _CRT_SECURE_NO_WARNINGS
#include "object.h"

bool Object::loadFromFile(const char* filename) {
  printf("Loading OBJ file %s...\n", filename);

  std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
  std::vector<glm::vec3> temp_vertices;
  std::vector<glm::vec2> temp_uvs;
  std::vector<glm::vec3> temp_normals;


  FILE * file = fopen(filename, "r");
  if (file == NULL){
    printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
    getchar();
    return false;
  }

  while (1) {
    char lineHeader[128];
    // read the first word of the line
    int res = fscanf(file, "%s", lineHeader);
    if (res == EOF)
      break; // EOF = End Of File. Quit the loop.

    // else : parse lineHeader

    if (strcmp(lineHeader, "v") == 0){
      glm::vec3 vertex;
      fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
      temp_vertices.push_back(vertex);
    } else if (strcmp(lineHeader, "vt") == 0){
      glm::vec2 uv;
      fscanf(file, "%f %f\n", &uv.x, &uv.y);
      uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
      temp_uvs.push_back(uv);
    } else if (strcmp(lineHeader, "vn") == 0){
      glm::vec3 normal;
      fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
      temp_normals.push_back(normal);
    } else if (strcmp(lineHeader, "f") == 0){
      std::string vertex1, vertex2, vertex3;
      unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
      int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
      if (matches != 9){
        printf("File can't be read by our simple parser :-( Try exporting with other options\n");
        return false;
      }
      vertexIndices.push_back(vertexIndex[0]);
      vertexIndices.push_back(vertexIndex[1]);
      vertexIndices.push_back(vertexIndex[2]);
      uvIndices.push_back(uvIndex[0]);
      uvIndices.push_back(uvIndex[1]);
      uvIndices.push_back(uvIndex[2]);
      normalIndices.push_back(normalIndex[0]);
      normalIndices.push_back(normalIndex[1]);
      normalIndices.push_back(normalIndex[2]);
    } else{
      // Probably a comment, eat up the rest of the line
      char stupidBuffer[1000];
      fgets(stupidBuffer, 1000, file);
    }

  }

  // For each vertex of each triangle
  for (unsigned int i = 0; i<vertexIndices.size(); i++){

    // Get the indices of its attributes
    unsigned int vertexIndex = vertexIndices[i];
    unsigned int uvIndex = uvIndices[i];
    unsigned int normalIndex = normalIndices[i];

    // Get the attributes thanks to the index
    glm::vec3 vertex = temp_vertices[vertexIndex - 1];
    glm::vec2 uv = temp_uvs[uvIndex - 1];
    glm::vec3 normal = temp_normals[normalIndex - 1];

    // Put the attributes in buffers
    this->vertex.push_back(vertex);
    this->uv.push_back(uv);
    this->normal.push_back(normal);
    this->color.push_back(glm::vec3(0.8f, 0.8f, 0.8f));
    this->color2.push_back(glm::vec3(0.1f, 0.1f, 0.1f));

  }

  return true;



}

static float distt(glm::vec2 pos, glm::vec4 vertex) {
  const float dx = pos.x - vertex.x;
  const float dy = pos.y - vertex.y;
  return dx*dx + dy*dy;
}

static bool is_point_near(glm::vec3 a, glm::vec3 b) {
  const float epsilon = 1e-6;
  return (abs(a.x - b.x) < epsilon && abs(a.y - b.y) < epsilon && abs(a.z - b.z) < epsilon);
}

size_t Object::findVertexAtMouse(glm::mat4 MV, glm::vec2 pos, float& dist) {
  glm::mat4 mvp = MV * model;

  size_t b = -1;
  float min_dist = 1e38;

  for (int i = 0; i < vertex.size(); i++) {
    auto r = mvp * glm::vec4(vertex[i],1);
    r /= r.w;

    auto dist = distt(pos, r);
    if (r.z >= 0.1f && r.z <= 100.f && dist < min_dist) {
      b = i;
      min_dist = dist;
    }
  }

  dist = min_dist;

  return b;
}

std::vector<size_t> Object::findAllSameVertex(size_t index) {
  std::vector<size_t> ret;
  for (size_t i = 0; i < vertex.size(); i++) {
    if (is_point_near(vertex[i], vertex[index]))
      ret.push_back(i);
  }
  return ret;
}

void Object::exportToPOV(FILE* fp, bool g) const {
  // Export as mesh2 archicture
  fprintf(fp, "mesh2 { \n");
  
  fprintf(fp, "\tvertex_vectors { \n");
  fprintf(fp, "\t\t%llu,\n", vertex.size());
  for (const auto& x : vertex) {
    fprintf(fp, "\t\t<%f, %f, %f>\n", x.x, x.y, x.z);
  }
  fprintf(fp, "\t}\n");

  fprintf(fp, "\tnormal_vectors { \n");
  fprintf(fp, "\t\t%llu,\n", normal.size());
  for (const auto& x : normal) {
    fprintf(fp, "\t\t<%f, %f, %f>\n", x.x, x.y, x.z);
  }
  fprintf(fp, "\t}\n");

  fprintf(fp, "\ttexture_list { \n");
  fprintf(fp, "\t\t%llu,\n", color.size());
  for (const auto& x : (isWireframe ? color2 : color)) {
    if (g)
      fprintf(fp, "\t\ttexture { pigment { rgbt <%f, %f, %f, 0.9> } finish { phong 1.0 } } \n", x.x, x.y, x.z);
    else
      fprintf(fp, "\t\ttexture { pigment { rgb <%f, %f, %f> } } \n", x.x, x.y, x.z);
  }
  fprintf(fp, "\t}\n");

  fprintf(fp, "\tface_indices  { \n");
  fprintf(fp, "\t\t%llu,\n", vertex.size()/3);
  for (int i = 0; i < vertex.size(); i += 3) {
    fprintf(fp, "\t\t<%d, %d, %d>, %d, %d, %d\n", i, i+1, i+2, i, i+1, i+2);
  }
  fprintf(fp, "\t}\n");

  fprintf(fp, "\tnormal_indices  { \n");
  fprintf(fp, "\t\t%llu,\n", vertex.size() / 3);
  for (int i = 0; i < vertex.size(); i += 3) {
    fprintf(fp, "\t\t<%d, %d, %d>\n", i, i + 1, i + 2);
  }
  fprintf(fp, "\t}\n");

  // Transformation matrices
  fprintf(fp, "\t material { interior { ior 1.5 } }\n\n");
  fprintf(fp, "\tmatrix <%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f>\n",
          model[0][0],
          model[0][1],
          model[0][2],
          model[1][0],
          model[1][1],
          model[1][2],
          model[2][0],
          model[2][1],
          model[2][2],
          model[3][0],
          model[3][1],
          model[3][2]
          );

  fprintf(fp, "}\n\n");
}
