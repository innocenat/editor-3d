
#include <windows.h>

bool showOpenFileDialog(char* output) {
  OPENFILENAME ofn;       // common dialog box structure
  char szFile[260];       // buffer for file name
  HANDLE hf;              // file handle


  // Initialize OPENFILENAME
  ZeroMemory(&ofn, sizeof(ofn));
  ofn.lStructSize = sizeof(ofn);
  ofn.hwndOwner = NULL;
  ofn.lpstrFile = szFile;
  ofn.lpstrFile[0] = '\0';
  ofn.nMaxFile = sizeof(szFile);
  ofn.lpstrFilter = "All\0*.*\0";
  ofn.nFilterIndex = 1;
  ofn.lpstrFileTitle = NULL;
  ofn.nMaxFileTitle = 0;
  ofn.lpstrInitialDir = NULL;
  ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
  if (GetOpenFileName(&ofn) == TRUE) {
    strcpy_s(output, 260, ofn.lpstrFile);
    return true;
  }
  return false;
}
